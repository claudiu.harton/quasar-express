const express = require("express");
const router = express.Router();
const { product, reset } = require("../controllers");

const { MongoClient } = require("mongodb");

const { mongo } = require("../configuration");

const findAll = async (req, res) => {
  const client = await MongoClient.connect(mongo.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).catch(err => {
    console.log(err);
  });

  try {
    const db = client.db("test");

    let collection = db.collection("products");
    res.send(await collection.find().toArray());
  } catch (err) {
    console.error(err);
  } finally {
    client.close();
  }
};
const r = async (req, res) => {
  const client = await MongoClient.connect(mongo.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).catch(err => {
    console.log(err);
  });

  try {
    const db = client.db("test");
    await db.collection("products").deleteMany({});
    await db.createCollection("products");

    res.send({ message: "DB Reseted" });
  } catch (err) {
    console.error(err);
  } finally {
    client.close();
  }
};

const addOne = async (req, res) => {
  const { name, category, quantity, price } = req.body;
  const client = await MongoClient.connect(mongo.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).catch(err => {
    console.log(err);
  });
  try {
    const db = client.db("test");

    let collection = db.collection("products");
    await collection.insertOne({ name, category, quantity, price });
    res.send({ message: "Product added" });
  } catch (err) {
    console.error(err);
  } finally {
    client.close();
  }
};

//reset
router.get("/reset", reset.reset);

//mongo
router.get("/", findAll);
router.post("/", addOne);
router.delete("/", r);

//get all
router.get("/products", product.getAll);

//get one
router.get("/products/:id", product.getOne);

//add one
router.post("/products", product.addOne);

//update one
router.put("/products/:id", product.updateOne);

//delete one
router.delete("/products/:id", product.deleteOne);

module.exports = router;
