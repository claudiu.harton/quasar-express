const { MongoClient } = require("mongodb");

const { mongo } = require("../configuration");

async function query(qf) {
  try {
    const db = await MongoClient.connect(mongo.link);
    await db.authenticate(mongo.user, mongo.password);

    const data = await qf({ ...arguments, db });
    await db.close();
    return data;
  } catch (e) {
    console.error(e);
  }
}

module.exports = {
  query
};
